import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Post } from '../interfaces/post';
import { Comment } from '../interfaces/comment';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  constructor(private postsService: PostsService,
    private authService:AuthService, 
    private router:Router,
    private route: ActivatedRoute) { }
  
  //geting posts from json not from db
  posts$: Post[];
  comments$: Comment[];
  userId:string;
  ans:string;
  like:number;
  numTo:number=0;

  myFunc(title:string, body:string, postId:number){
    this.like=0;
  this.postsService.addPost(this.userId, title, body, postId, this.like);
  this.numTo=postId;
  this.ans ="Saved for later viewing"
  }

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
      this.userId = user.uid;
      })
        this.postsService.getPosts().subscribe(data => this.posts$ = data)
        this.postsService.getComment().subscribe(data => this.comments$ = data)
  }

}