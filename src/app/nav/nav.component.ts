import { AuthService } from './../auth.service';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Location } from "@angular/common";
import { Router } from "@angular/router";


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  title: string = 'Test';
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, location: Location, public authService:AuthService, router: Router){
    router.events.subscribe(val => {
      // if (location.path() == "/generals" || location.path() == "/generalsform") {
      //   this.title = 'generals';      
      // } else 
      if (location.path() == "/signup"){
        this.title = "Sign up form";
      } else if (location.path() == "/login"){
        this.title = "Login form";
      } else if (location.path() == "/posts"){
        this.title = "Blog Posts";
      } else if (location.path() == "/displays"){
        this.title = "My Posts";
      } else{
        this.title = "Test 20";
      }
    });   
  }
}
