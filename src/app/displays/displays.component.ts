import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-displays',
  templateUrl: './displays.component.html',
  styleUrls: ['./displays.component.css']
})
export class DisplaysComponent implements OnInit {

  constructor(private postsService: PostsService,
    private authService:AuthService, 
    private router:Router,
    private route: ActivatedRoute) { }

    disps$:Observable<any[]>;
  userId:string;
  id:string;

  deleteGeneral(id){
    this.postsService.deletePost(this.userId,id)
    console.log(id);
  }
  
  addLikes(id:string, likes:number){
      this.postsService.addLike(this.userId,id,likes)
    }

  ngOnInit() {
    
this.id = this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.disps$ = this.postsService.getPost(this.userId); 
      }
    )
  }

}
