import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { ClassifiedService } from '../classified.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  constructor(private classifiedService:ClassifiedService,
              public authService:AuthService) { }

  panelOpenState = false;
  articles$:Observable<any[]>;

  deleteArticle(id:string){
    this.classifiedService.deleteArticle(id);
  }


  ngOnInit() { 
    this.articles$ = this.classifiedService.getArticles();
  }

}
