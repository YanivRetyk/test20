
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Post } from './interfaces/post';
import { Comment } from './interfaces/comment';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {


  apiUrl='https://jsonplaceholder.typicode.com/posts';
  apiUrlpho='https://jsonplaceholder.typicode.com/posts/1/comments';

  constructor(private http: HttpClient, 
    private db: AngularFirestore,
    private router:Router,
    private authService:AuthService) { }
    
    userCollection:AngularFirestoreCollection = this.db.collection('users');
    postCollection:AngularFirestoreCollection

// GETING FROM JSON
getPosts(){
  return this.http.get<Post[]>(this.apiUrl)
}
getComment(){
  return this.http.get<Comment[]>(this.apiUrlpho)
}

getPost(userId): Observable<any[]> {
  //const ref = this.db.collection('generals');
  //return ref.valueChanges({idField: 'id'});
  this.postCollection = this.db.collection(`users/${userId}/posts`);
  console.log('Generals collection created');
  return this.postCollection.snapshotChanges().pipe(
    map(collection => collection.map(document => {
      const data = document.payload.doc.data();
      data.id = document.payload.doc.id;
      return data;
    }))
  );    
} 

deletePost(userId:string, id:string){
  this.db.doc(`users/${userId}/posts/${id}`).delete();
}

// addPhoto(comp:boolean,  title:String){
//   const todo = {title:title, completed:comp}
//   this.db.collection('todos').add(todo)  
//   .then(
//     res =>  
//     {
//   this.router.navigate(['/todos']);
//     }
//   )
// }  

addLike(userId:string, id:string, like:number){
  like++;
  console.log(like);
  this.db.doc(`users/${userId}/posts/${id}`).update(
    {
      likes:like
    }
   ) 
   console.log('addddd');
}


addPost(userId:string, title:string,body:string, postId:number, like:number){
  console.log('In add post');
  const post = {postId:postId, title:title,body:body, likes:like}
  this.userCollection.doc(userId).collection('posts').add(post);
} 

}